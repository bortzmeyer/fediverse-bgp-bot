#!/usr/bin/env python3

# A Fediverse (decentralized social network, for instance using Mastodon) bot
# to answer to requests about IP prefixes, returning the origin AS.
# Stéphane Bortzmeyer bortzmeyer@afnic.fr

# Defaults that you will probably change
MYNAME = "bgp"
MYDOMAIN = "botsin.space"
HOME="/home/whichasn/" # Don't forget the slash at the end
SOCKFILE="/home/stephane/run/whichasn.socket"
BLACKLIST = ["badguy@mastodon.example", "evilinstance.example"] # You can add accounts or entire instances.
# TODO: canonicalize the case?

# Defaults to you may change
TIMEOUT = 5 # seconds
LOGGER = MYNAME
MAXLOG = 100 # Maximum size of answers to log
SLEEP_DELAY = 0.75 # Sleep time between BGP requests
RESTART_DELAY = 0.5 # To avoid restart crazy loops
TIMER_DELAY = 9 
DEBUG=False

# Defaults that are better left alone
MASTODON_MAX = 500 # This is Mastodon default size. Other fediverse programs
                   # have a different limit (5 000 by default for Pleroma) but
                   # we have to be conservative, to serve everybody.
MAXIMUM_HEARTBEAT_DELAY = 50 # Heartbeats are sent every 15 seconds

# Documentation in <https://mastodonpy.readthedocs.io/en/latest/> Note it
# should work with all the programs using the Mastodon API, such as Pleroma,
# but it was not tested yet.
from mastodon import Mastodon, StreamListener

# http://lxml.de/
from lxml import html

import re
import logging
import sys
import time
import string
import socket
import os
import fcntl
import multiprocessing
import tempfile

class myListener(StreamListener):

    def __init__(self, masto, log, hb_filename, pid_filename):
        self.hb_filename = hb_filename
        self.pid_filename = pid_filename
        self.blacklist = BLACKLIST
        self.blacklist.append("%s@%s" % (MYNAME, MYDOMAIN)) # To avoid loops
        self.masto = masto
        self.log = log
        self.pid_file = open(self.pid_filename, 'w')
        fcntl.lockf(self.pid_file, fcntl.LOCK_EX)
        print("%s" % os.getpid(), file=self.pid_file)
        fcntl.lockf(self.pid_file, fcntl.LOCK_UN)
        self.pid_file.close()
        self.heartbeat_file = open(self.hb_filename, 'w')
        fcntl.lockf(self.heartbeat_file, fcntl.LOCK_EX)
        print(time.time(), file=self.heartbeat_file)
        fcntl.lockf(self.heartbeat_file, fcntl.LOCK_UN)
        self.heartbeat_file.close()
        super()

    def handle_heartbeat(self):
        self.log.debug("HEARTBEAT") # Sent every 15 seconds by Mastodon        
        self.heartbeat_file = open(self.hb_filename, 'w')
        fcntl.lockf(self.heartbeat_file, fcntl.LOCK_EX)
        print(time.time(), file=self.heartbeat_file)
        fcntl.lockf(self.heartbeat_file, fcntl.LOCK_UN)
        self.heartbeat_file.close()
                                                                    
    def on_notification(self, notification):
        # The bot is strictly sequential, handling one notification
        # after the other. This is not a problem since the WhichASN
        # daemon always reply in constant-time (except when reloadiung
        # its database).
        try:
            sender = None
            visibility = None
            if notification['type'] == 'mention':
                id = notification['status']['id']
                sender = notification['account']['acct']
                if sender in BLACKLIST:
                    self.log.info("Service refused to %s" % sender)
                    return
                match = re.match("^.*@(.*)$", sender)
                if match:
                    sender_domain = match.group(1)
                    if sender_domain in BLACKLIST:
                        self.log.info("Service refused to %s" % sender)
                        return
                else:
                    # Probably local instance, without a domain name. Note that we cannot blacklist local users.
                    if sender == MYNAME:
                        self.log.info("Loop detected, sender is myself")
                        return
                # TODO Rate-limit per-user?
                visibility = notification['status']['visibility']
                # Mastodon API returns the content of the toot in
                # HTML, just to make our lifes miserable
                doc = html.document_fromstring(notification['status']['content'])
                # Preserve end-of-lines
                # <https://stackoverflow.com/questions/18660382/how-can-i-preserve-br-as-newlines-with-lxml-html-text-content-or-equivalent>
                for br in doc.xpath("*//br"):
                            br.tail = "\n" + br.tail if br.tail else "\n"
                for p in doc.xpath("*//p"):
                            p.tail = "\n" + p.tail if p.tail else "\n"
                body = doc.text_content()
                match = re.match("^@%s\s+(.+)$" % MYNAME, body)
                if match:
                    prefix = match.group(1)                
                    try:
                        self.sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
                        self.sock.connect(SOCKFILE)
                        self.sock.sendall(bytes(prefix + "\n", "utf-8"))
                        msg = str(self.sock.recv(4096), "utf-8")
                        msgs = None
                        if msg == "":
                            msg = "NODATA - No route found for %s" % prefix
                        elif not msg.startswith("I don't understand") and \
                             not msg.startswith("Send just") and not msg.startswith("OK:"):
                            rprefix = None
                            rasn = None
                            try:
                                (rprefix, rasn) = msg.split(maxsplit=1)
                            except ValueError:
                                self.log.error("No tuple prefix,ASN in \"msg\"")
                            # Unfortunately, there is no way in
                            # Mastodon.py (or in the Mastodon API?) to
                            # send HTML. So we have to send raw links
                            # at the end
                            if rprefix is not None and rasn is not None:
                                msgs = msg
                                rasns = rasn.split()
                                if len(rasns) == 1:
                                    msg += "\n\nDetails: On prefix: https://stat.ripe.net/ui2013/%s On AS: https://stat.ripe.net/ui2013/AS%s" % \
                                        (rprefix, rasn)
                                else:
                                    msg += "\n\nDetails: On prefix: https://stat.ripe.net/ui2013/%s On ASes: " % rprefix
                                    for rasn in rasns:
                                        msg += "https://stat.ripe.net/ui2013/AS%s " % rasn
                        else:
                            pass
                    except ConnectionRefusedError:
                         msg = "ERROR Cannot connect to my WhichASN server"
                    except Exception as error: # TODO may be too revealing on internal
                        # implementation (but this is free software, after all)
                        msg = "ERROR General failure: %s" % error
                    finally:
                        self.sock.close()
                    if len(msg) >= MASTODON_MAX:
                        msg = "@%s Sorry, answer is %d characters, too large for Mastodon" % (sender, len (msg))
                    if msgs is None:
                        msgs = msg
                    text = "@%s %s" % (sender, msg)
                    try:
                        self.masto.status_post(text, in_reply_to_id = id, 
                                               visibility = visibility)
                        self.log.info("%s -> %s - %s" % (sender, msgs, visibility))                     
                    except Exception as error:
                        self.log.error("Cannot post to %s: %s" % (sender, error))
                else:
                    self.log.error("%s : \"%s\" does not match" % (sender, body))
                    pass # Produces an error message, at least? Be
                         # careful not to answer if we are just
                         # included in a conversation. Or log it
                         # (dangerous if toot-bombing)?
            time.sleep(SLEEP_DELAY) # Crude rate-limiting 
        except KeyError as error:
            self.log.error("Malformed notification, missing %s" % error)
        except Exception as error: 
            self.log.error("%s -> %s" % (sender, error))

def driver(log, heartbeat_filename, pid_filename):
    try:
        mastodon = Mastodon(
            client_id = "%s_clientcred.secret" % MYNAME,
            access_token = "%s_usercred.secret" % MYNAME,
            api_base_url = "https://%s/" % MYDOMAIN,
            debug_requests = DEBUG)
        listener = myListener(mastodon, log, heartbeat_filename, pid_filename)
        log.info("Driver/listener starts, PID %s" % os.getpid())
        if not mastodon.stream_healthy():
            log_critical("Streaming server is not healthy")
            return False
        mastodon.stream_user(listener)
    except Exception as error:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        log.critical("Unexpected error %s in the driver \"%s\"" % (exc_type.__name__, error))

log = logging.getLogger("LOGGER")
channel = logging.StreamHandler(sys.stdout)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
channel.setFormatter(formatter)
log.addHandler(channel)
log.setLevel(logging.INFO)
while True:
    try:
        tmp_hb_filename = tempfile.mkstemp(prefix=HOME + "whichasn-bot-", suffix='.heartbeat')
        tmp_pid_filename = tempfile.mkstemp(prefix=HOME + "whichasn-bot-", suffix='.pid')
        log.debug("Creating a new process")
        proc = multiprocessing.Process(target=driver, args=(log, tmp_hb_filename[1],tmp_pid_filename[1]))
        proc.start()
        while True:
            time.sleep(TIMER_DELAY)
            if proc.is_alive():
                h = open(tmp_hb_filename[1], 'r')
                fcntl.lockf(h, fcntl.LOCK_SH)
                data = h.read(128)
                fcntl.lockf(h, fcntl.LOCK_UN)
                h.close()
                try:
                    last_heartbeat = float(data)
                except ValueError: # TODO there is a bug somewhere
                                   # since we sometimes get empty
                                   # heartbeat files. That's strange
                                   # since it is supposed to be filled
                                   # in when the listener starts, even
                                   # before the first heartbeat is
                                   # received. May be establishing the
                                   # session failed, triggering an
                                   # exception, and leaving the file
                                   # unfilled?
                    log.error("Invalid value in heartbeat file %s: \"%s\"" % (tmp_hb_filename[1], data))
                    last_heartbeat = 0
                if time.time() - last_heartbeat > MAXIMUM_HEARTBEAT_DELAY:
                    log.error("No more heartbeats, kill %s" % proc.pid)
                    proc.terminate()
                    proc.join()
                    log.debug("Done, it exited with code %s" % proc.exitcode)
                    try:
                        os.remove(tmp_hb_filename[1])
                    except:
                        pass
                    try:
                        os.remove(tmp_pid_filename[1])
                    except:
                        pass
                    break
            else:
                log.error("Bot died, we restart it")
                time.sleep(RESTART_DELAY)
                try:
                    os.remove(tmp_hb_filename[1])
                except:
                    pass
                try:
                    os.remove(tmp_pid_filename[1])
                except:
                    pass
                break
    except Exception as error:
        log.critical("Unexpected error in the timer: \"%s\"" % error)
        try: # Try to clean
            proc.terminate()
            proc.join()
            try:
                os.remove(tmp_hb_filename[1])
            except:
                pass
            try:
                os.remove(tmp_pid_filename[1])
            except:
                pass
        except:
            pass
        time.sleep(RESTART_DELAY)
                                                                                                                                                                                       
