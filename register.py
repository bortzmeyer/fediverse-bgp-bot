#!/usr/bin/env python3

MYACCOUNT = 'foo@bar.example' # Your account
MYURL = 'https://mastodon.example' # Your Mastodon instance (should also work with all implementations using the Mastodon API, such as Pleroma)

from mastodon import Mastodon
import sys

if len(sys.argv) != 2:
    raise Exception("Usage: %s password" % sys.argv[0])
password = sys.argv[1]

# Register app - only once!
Mastodon.create_app(
     'BGP',
     api_base_url = MYURL,
     to_file = 'bgp_clientcred.secret'
)

# Log in
mastodon = Mastodon(
    client_id = 'bgp_clientcred.secret',
    api_base_url = MYURL
)
mastodon.log_in(
    MYACCOUNT,
    password,
    to_file = 'bgp_usercred.secret'
)

