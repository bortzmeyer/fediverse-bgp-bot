# fediverse-BGP-bot

A bot for the fediverse (Mastodon and others), answering questions
about the BGP default-free table (which AS - Autonomous System -
originates this IP prefix).

## Usage 

An instance of the bot runs at `@bgp@botsin.space`. Just write to
it an IP address or an IP prefix. An exemple is [viewable
online](https://mastodon.gougere.fr/@bortzmeyer/103068801388745111) or
here:

![](query.png)

The reply incluses links to [RIPE Stat](https://stat.ripe.net/) to get more information
about the announced prefix, and the AS (Autonomous System).

If you use the excellent
[madonctl tool](https://github.com/McKael/madonctl), you can send
toots on the command line. Here is one example:

```
madonctl toot --visibility direct "@bgp@botsin.space 185.167.17.10"
```

If you want to have information about less-specific prefixes, just add
'A' (for All) at the end of the request, after the prefix.

## Installation

1. Install the [WhichASN](https://framagit.org/bortzmeyer/whichasn) daemon on the machine that will run the
   fediverse bot.
2. Create an account on a Mastodon instance (it may works with
   Pleroma, too, but it has not been tested). Do not forget to flag
   your account as a bot.
3. Modify `register.py` and `bot.py` to match your account and your preferences.
4. Install the [Mastodon.py](https://github.com/halcy/Mastodon.py/)
   library, and the [lxml](http://lxml.de/) library.
5. Register the application with the `register.py` script.
6. Start the bot `bot.py`.

## About the Fediverse and Mastodon

The Fediverse is the generic name of the decentralized social network
made of all the instances running compatible protocols, such as
ActivityPub. [Mastodon](https://joinmastodon.org/) is one of the
implementations, built on free software. Users can send short
messages, called "toots".

## Licence

See the file LICENSE

## Author

Stéphane Bortzmeyer <stephane+framagit@bortzmeyer.org>

## Reference site

[On FramaGit](https://framagit.org/bortzmeyer/fediverse-bgp-bot)

## See also

A bot for DNS questions <https://framagit.org/bortzmeyer/mastodon-DNS-bot/>
